package com.h2moko.testmoko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestMokoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestMokoApplication.class, args);
	}

}
