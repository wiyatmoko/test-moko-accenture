package com.h2moko.testmoko.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user_setting")
@Entity
@EntityListeners(AuditingEntityListener.class)
public class UserSetting {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @CreatedBy
    @Column(name = "created_by", nullable = false)
    private String createdBy;

    @CreatedDate
    @Column(name = "created_time", nullable = false)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "updated_by")
    private String updatedBy;

    @LastModifiedDate
    @Column(name = "updated_time")
    private Date updatedDate;


    @Column(name = "deleted_time")
    private Date deletedDate;

    @Column(name = "key",nullable = false)
    private String keyUserSetting;

    @Column(name = "value",nullable = false)
    private String valueUserSetting;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private CategorySetting categorySetting;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users users;

}
