package com.h2moko.testmoko.users.application;

import com.h2moko.testmoko.entity.CategorySetting;
import com.h2moko.testmoko.entity.UserSetting;
import com.h2moko.testmoko.users.web.in.*;
import com.h2moko.testmoko.users.web.out.GetUserSpesification;
import com.h2moko.testmoko.utils.exceptions.AlreadyExistException;
import com.h2moko.testmoko.utils.exceptions.NotFoundException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import com.h2moko.testmoko.entity.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserSettingRepository userSettingRepository;

    @Autowired
    CategorySettingRepository categorySettingRepository;

    @Override
    @Transactional
    public Page<Users> getAllPage(int maxRecord, int offset) {
        return userRepository.findAll(GetUserSpesification.builder()
                .build(),PageRequest.of(offset, maxRecord));
    }


    @Override
    @Transactional
    public List<Users> getAll() {
        return userRepository.getAllByDeletedDateIsNull();
    }

    @Override
    @Transactional
    public Users create(UserCreateRequest request) throws AlreadyExistException {
        Optional<Users> userExistingSsn = userRepository.getBySsnAndDeletedDateIsNull(request.getSsn());
        if(userExistingSsn.isPresent()){
            throw new AlreadyExistException("Users ssn already exist");
        }

        Users users = Users.builder()
                .firstName(request.getFirst_name())
                .middleName(request.getLast_name())
                .ssn(request.getSsn())
                .isActive(true)
                .createdBy("SYSTEM")
                .updatedBy("SYSTEM")
                .createdDate(new Date())
                .updatedDate(new Date())
                .birthDate(request.getBirth_date())
                .build();
        Users userExisting = userRepository.save(users);
        List<UserSetting> userSettingList = createUserSetting(userExisting);
        userExisting.setUserSettings(userSettingList);
        return userExisting;
    }

    @SneakyThrows
    @Override
    public Users getDetail(Long id) {
        Optional<Users> usersExisting = userRepository.getByIdAndDeletedDateIsNull(id);
        if(!usersExisting.isPresent()){
            throw new NotFoundException("Users id not found : " + id);
        }
        return usersExisting.get();
    }

    @SneakyThrows
    @Override
    public Users update(Long id, UserCreateRequest request) {
        Optional<Users> usersExisting = userRepository.getByIdAndDeletedDateIsNull(id);
        if(usersExisting.isPresent()){
            usersExisting.get().setFirstName(request.getFirst_name());
            usersExisting.get().setMiddleName(request.getLast_name());
            usersExisting.get().setSsn(request.getSsn());
            usersExisting.get().setBirthDate(request.getBirth_date());
        } else {
            throw new NotFoundException("Users id not found");
        }
        return userRepository.save(usersExisting.get());
    }

    @SneakyThrows
    @Override
    public void delete(Long id) {
        Optional<Users> usersExisting = userRepository.findById(id);
        if(usersExisting.isPresent()){
            usersExisting.get().setActive(false);
            usersExisting.get().setDeletedDate(new Date());
            userRepository.save(usersExisting.get());
        } else {
            throw new NotFoundException("Users id not found");
        }

    }

    @SneakyThrows
    @Override
    public Users refresh(Long id)  {
        Optional<Users> usersExisting = userRepository.findById(id);
        if(usersExisting.isPresent()){
            usersExisting.get().setActive(true);
            usersExisting.get().setDeletedDate(null);

        } else {
            throw new NotFoundException("Users id not found");
        }

        return userRepository.save(usersExisting.get());
    }

    @SneakyThrows
    @Override
    public Users setting(Long id, Map<String, String>[] request) {
        Optional<Users> usersExisting = userRepository.findById(id);
        if(!usersExisting.isPresent()){
            throw new NotFoundException("Users id not found");
        }
        Users users = usersExisting.get();
        Map<CategorySetting, List<UserSetting>> userSettingListMap = null;
        if(users.getUserSettings() != null) {
            userSettingListMap  = users.getUserSettings().stream().collect(Collectors.groupingBy(UserSetting::getCategorySetting));
            for (Map.Entry<CategorySetting,List<UserSetting>> userSettingList: userSettingListMap.entrySet()){
                userSettingList.getValue().forEach(userSetting1 -> {
                    for(Map<String,String> req: Arrays.stream(request).toList()){
                        log.info(req.get(userSetting1.getCategorySetting().getName()));
                        if(req.containsKey(userSetting1.getCategorySetting().getName())){
                            String str = req.values().toString();
                            String strNew = str.replaceAll("]", "");
                            String strNews = strNew.replace("[", "");
                            userSetting1.setValueUserSetting(strNews);
                            userSettingRepository.save(userSetting1);
                        }
                    }
                });
            }
        }

        return users;
    }

    private List<UserSetting> createUserSetting(Users user) {
        List<CategorySetting> categorySettingExisting = categorySettingRepository.findAllByDeletedDateIsNull();
        List<UserSetting> userSettingList = new ArrayList<>();
        if(categorySettingExisting != null){
            categorySettingExisting.forEach(categorySetting -> {
                userSettingList.add(UserSetting.builder()
                        .valueUserSetting(categorySetting.getCode().equals("widget_order") ? "1,2,3,4,5,6" : "false")
                        .keyUserSetting("false")
                        .users(user)
                        .createdBy("SYSTEM")
                        .updatedBy("SYSTEM")
                        .createdDate(new Date())
                        .updatedDate(new Date())
                        .categorySetting(categorySetting)
                        .build());
            });
        }
        userSettingRepository.saveAll(userSettingList);
        return userSettingList;
    }
}
