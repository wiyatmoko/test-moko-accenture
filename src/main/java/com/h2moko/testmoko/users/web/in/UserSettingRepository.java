package com.h2moko.testmoko.users.web.in;

import com.h2moko.testmoko.entity.CategorySetting;
import com.h2moko.testmoko.entity.UserSetting;
import com.h2moko.testmoko.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserSettingRepository extends JpaRepository<UserSetting, Long> {

    UserSetting getByCategorySetting(CategorySetting categorySetting);
}
