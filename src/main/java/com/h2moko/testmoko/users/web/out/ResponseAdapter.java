package com.h2moko.testmoko.users.web.out;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseAdapter<T> {

    private T user_data;
    private long max_records;
    private long offset;

    public static <T> ResponseAdapter<T> ok(T data, Long total, Long maxRecord) {
        ResponseAdapter<T> response = new ResponseAdapter();
        response.user_data = data;
        response.max_records = maxRecord;
        response.offset = total;
        return response;
    }
}
