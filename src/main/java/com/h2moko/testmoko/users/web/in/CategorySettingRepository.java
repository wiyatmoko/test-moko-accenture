package com.h2moko.testmoko.users.web.in;

import com.h2moko.testmoko.entity.CategorySetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategorySettingRepository extends JpaRepository<CategorySetting, Long> {

    List<CategorySetting> findAllByDeletedDateIsNull();
    List<CategorySetting> findAllByDeletedDateIsNullAndCode(String code);
    CategorySetting getByCode(String code);
}
