package com.h2moko.testmoko.users.web.out;

import com.h2moko.testmoko.entity.Users;
import com.h2moko.testmoko.entity.Users_;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.Builder;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

@Builder
public class GetUserSpesification implements Specification<Users> {
    @Override
    public Predicate toPredicate(Root<Users> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.isNull(root.get(Users_.DELETED_DATE)));
        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
