package com.h2moko.testmoko.users.web.in;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class UserCreateRequest {
    private String ssn;
    private String first_name;
    private String last_name;
    private Date birth_date;
}
