package com.h2moko.testmoko.users.web.in;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@Builder
public class UpdateUserSettingRequest {
    private List<Map<String, String>> user_setting;
}
