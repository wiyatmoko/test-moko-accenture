package com.h2moko.testmoko.users.web.in;

import com.h2moko.testmoko.entity.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> , JpaSpecificationExecutor<Users> {
    List<Users> getAllByDeletedDateIsNull();

    Page<Users> getAllByDeletedDateIsNull(Pageable pageable);

    Optional<Users> getByIdAndDeletedDateIsNull(Long id);
    Optional<Users> getBySsnAndDeletedDateIsNull(String ssn);
}
