package com.h2moko.testmoko.users.web.in;

import com.h2moko.testmoko.entity.Users;
import com.h2moko.testmoko.utils.exceptions.AlreadyExistException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface UserService {
    List<Users> getAll();
    Users create(UserCreateRequest request) throws AlreadyExistException;

    Users getDetail(Long id);

    Users update(Long id, UserCreateRequest request);

    void delete(Long id);

    Users refresh(Long id);

    Users setting(Long id, Map<String, String>[] request);

    Page<Users> getAllPage(int maxRecord, int offset);
}
