package com.h2moko.testmoko.users.web.out;

import com.h2moko.testmoko.entity.CategorySetting;
import com.h2moko.testmoko.entity.UserSetting;
import com.h2moko.testmoko.entity.Users;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Builder
public class ResponseUsers {
    private UserResponse user_data;
    private List<Map<String, String>> user_setting;

    public static ResponseUsers fromEntity(Users users){
        Map<CategorySetting, List<UserSetting>> userSettingListMap = null;
        List<Map<String, String>> userSettingMap = new ArrayList<>();
        if(users.getUserSettings() != null) {
            userSettingListMap  = users.getUserSettings().stream().collect(Collectors.groupingBy(UserSetting::getCategorySetting));
            for (Map.Entry<CategorySetting,List<UserSetting>> userSettingList: userSettingListMap.entrySet()){
                userSettingList.getValue().forEach(userSetting1 -> {
                    Map<String, String> stringMap = new HashMap<>();
                    stringMap.put(userSetting1.getCategorySetting().getName(), userSetting1.getValueUserSetting());
                    userSettingMap.add(stringMap);
                });
            }
        }
        return ResponseUsers.builder()
                .user_data(UserResponse.fromEntity(users))
                .user_setting(userSettingMap != null ?userSettingMap : null)
                .build();
    }
}
