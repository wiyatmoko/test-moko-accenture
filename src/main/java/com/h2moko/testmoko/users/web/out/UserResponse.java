package com.h2moko.testmoko.users.web.out;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.h2moko.testmoko.entity.Users;
import com.h2moko.testmoko.utils.CustomDateSerializer;
import com.h2moko.testmoko.utils.CustomDateSerializerBirtdate;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class UserResponse  {

    private Long id;
    private String firstName;
    private String middleName;
    private String familyName;
    private String ssn;
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date created_time;
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date updated_time;
    private String created_by;
    private String updated_by;
    private boolean isActive;
    @JsonSerialize(using = CustomDateSerializerBirtdate.class)
    private Date birth_date;

    public static UserResponse fromEntity(Users users){
        return UserResponse.builder()
                .firstName(users.getFirstName())
                .id(users.getId())
                .ssn(users.getSsn())
                .middleName(users.getMiddleName())
                .familyName(users.getFamilyName())
                .created_time(users.getCreatedDate())
                .updated_time(users.getUpdatedDate())
                .updated_by(users.getUpdatedBy())
                .created_by(users.getCreatedBy())
                .isActive(users.isActive())
                .birth_date(users.getBirthDate())
                .build();
    }
}
