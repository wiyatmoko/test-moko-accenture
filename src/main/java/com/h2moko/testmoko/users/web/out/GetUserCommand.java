package com.h2moko.testmoko.users.web.out;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Sort;

@Getter
@Setter
@Builder
public class GetUserCommand {
    private int page;
    private int size;
    private String sortBy;
    private Sort.Direction direction;
}
