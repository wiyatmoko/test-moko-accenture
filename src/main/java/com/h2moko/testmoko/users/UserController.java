package com.h2moko.testmoko.users;

import com.h2moko.testmoko.users.web.in.UserCreateRequest;
import com.h2moko.testmoko.users.web.in.UserService;
import com.h2moko.testmoko.users.web.out.ResponseAdapter;
import com.h2moko.testmoko.users.web.out.ResponseUsers;
import com.h2moko.testmoko.users.web.out.UserResponse;
import com.h2moko.testmoko.utils.exceptions.AlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.h2moko.testmoko.entity.Users;

import java.util.Map;

@RestController
@RequestMapping(value="v1/users")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping()
    public ResponseEntity<?> create(@RequestBody UserCreateRequest request) throws AlreadyExistException {

        Users users = userService.create(request);
        return ResponseEntity.ok(ResponseUsers.fromEntity(users));
    }


    @GetMapping("/{id}")
    public ResponseEntity<?>  getDetail(@PathVariable ("id") Long id) {
        Users users  = userService.getDetail(id);
        return ResponseEntity.ok(ResponseUsers.fromEntity(users));
    }
    @GetMapping()
    public ResponseAdapter<?> getAllPage(@RequestParam("max_records") int maxRecord,
                                         @RequestParam("offset") int offset) {

        Page<Users> userListPage  = userService.getAllPage(maxRecord,offset);
        return ResponseAdapter.ok(userListPage.getContent().stream().map(users -> UserResponse.fromEntity(users)), (long) userListPage.getTotalPages(),(long) userListPage.getContent().size());
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable ("id") Long id,
                                    @RequestBody UserCreateRequest request){
        Users users  = userService.update(id, request);
        return ResponseEntity.ok(ResponseUsers.fromEntity(users));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable ("id") Long id){
      userService.delete(id);
    }


    @PutMapping("/{id}/refresh")
    public ResponseEntity<?> refresh(@PathVariable ("id") Long id){
        Users users  = userService.refresh(id);
        return ResponseEntity.ok(ResponseUsers.fromEntity(users));
    }

    @PutMapping("/{id}/settings")
    public ResponseEntity<?> setting(@PathVariable ("id") Long id, @RequestBody Map<String, String>[] request){
        Users users  = userService.setting(id,request);
        return ResponseEntity.ok(ResponseUsers.fromEntity(users));
    }

}
