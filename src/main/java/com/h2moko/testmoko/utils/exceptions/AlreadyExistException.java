package com.h2moko.testmoko.utils.exceptions;

public class AlreadyExistException extends Exception{
    public AlreadyExistException(String message) {
        super(message);
    }
}
