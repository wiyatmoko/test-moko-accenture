package com.h2moko.testmoko.utils.exceptions;

public class InvalidValueFieldException extends Exception {
    public  InvalidValueFieldException(String message) {
        super(message);
    }
}
