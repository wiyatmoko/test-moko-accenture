package com.h2moko.testmoko.utils.exceptions;

public class NotFoundException extends Exception{
    public NotFoundException(String message) {
        super(message);
    }
}