package com.h2moko.testmoko.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseAdapterResponse{
    private String status;
    private Integer code;
    private String message;


    public static  BaseAdapterResponse nok(HttpStatus httpStatus, Exception exception, int code) {
        BaseAdapterResponse response = new BaseAdapterResponse();
        response.code = code;
        response.message = exception.getMessage();
        response.status = "" + httpStatus;
        return response;
    }

}
