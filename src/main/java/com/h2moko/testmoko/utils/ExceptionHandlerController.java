package com.h2moko.testmoko.utils;

import com.h2moko.testmoko.utils.exceptions.AlreadyExistException;
import com.h2moko.testmoko.utils.exceptions.InvalidValueFieldException;
import com.h2moko.testmoko.utils.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = { Exception.class })
    protected ResponseEntity handleAllException(
            Exception ex, WebRequest request) {
        int code = 80000;
        BaseAdapterResponse bodyOfResponse = BaseAdapterResponse.nok(HttpStatus.INTERNAL_SERVER_ERROR, ex,code);
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        if (ex instanceof AlreadyExistException) {
            code = 30001;
            bodyOfResponse = BaseAdapterResponse.nok(HttpStatus.CONFLICT, ex, code);
            status = HttpStatus.CONFLICT;

        } else if (ex instanceof NotFoundException) {
            code = 30000;
            bodyOfResponse = BaseAdapterResponse.nok(HttpStatus.NOT_FOUND, ex, code);
            status = HttpStatus.NOT_FOUND;

        } else if (ex instanceof InvalidValueFieldException) {
            code = 30002;
            bodyOfResponse = BaseAdapterResponse.nok(HttpStatus.UNPROCESSABLE_ENTITY, ex, code);
            status = HttpStatus.UNPROCESSABLE_ENTITY;
        }
        return handleExceptionInternal(ex, bodyOfResponse,
                new HttpHeaders(), status, request);
    }
}
