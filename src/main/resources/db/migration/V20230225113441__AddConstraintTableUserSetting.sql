ALTER TABLE user_setting
    ADD category_id bigint not null;
ALTER TABLE user_setting ADD CONSTRAINT user_category_setting_id_fk FOREIGN KEY (category_id) REFERENCES category_setting(id);