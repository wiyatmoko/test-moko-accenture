CREATE TABLE users (
                       id bigint not null,
                       created_by varchar(255) NOT NULL DEFAULT 'SYSTEM',
                       created_time timestamp NOT NULL,
                       updated_by varchar(255) NOT NULL DEFAULT 'SYSTEM',
                       updated_time timestamp NULL,
                       deleted_time timestamp NULL,
                       ssn varchar(16) NOT NULL,
                       first_name varchar(255) NOT NULL,
                       middle_name varchar(255) NOT NULL,
                       family_name varchar(255) NULL,
                       birth_date date NOT NULL,
                       is_active boolean NOT NULL DEFAULT true,
                       CONSTRAINT users_pkey PRIMARY KEY (id)
);