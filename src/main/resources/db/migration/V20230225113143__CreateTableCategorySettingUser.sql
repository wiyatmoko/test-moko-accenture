CREATE TABLE category_setting (
    id bigint not null,
    created_by varchar(255) NOT NULL DEFAULT 'SYSTEM',
    created_time timestamp NOT NULL,
    updated_by varchar(255) NOT NULL DEFAULT 'SYSTEM',
    updated_time timestamp NULL,
    deleted_time timestamp NULL,
    "name" varchar(100) NOT NULL,
    "code" varchar(100) NOT NULL,
    CONSTRAINT category_setting_pkey PRIMARY KEY (id)
);