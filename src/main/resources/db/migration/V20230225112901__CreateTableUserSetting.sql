CREATE TABLE user_setting (
        id bigint not null,
        created_by varchar(255) NOT NULL DEFAULT 'SYSTEM',
        created_time timestamp NOT NULL,
        updated_by varchar(255) NOT NULL DEFAULT 'SYSTEM',
        updated_time timestamp NULL,
        deleted_time timestamp NULL,
        "key" varchar(100) NOT NULL,
        "value" varchar(100) NOT NULL,
        user_id bigint NOT NULL,
        CONSTRAINT user_setting_pkey PRIMARY KEY (id)
);

ALTER TABLE user_setting ADD CONSTRAINT user_setting_id_fk FOREIGN KEY (user_id) REFERENCES users(id);