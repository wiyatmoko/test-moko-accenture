INSERT INTO category_setting (id,created_time,updated_time,created_by,updated_by,"name","code")
VALUES (1,now(),now(),'SYSTEM','SYSTEM','biometric_login','biometric_login');

INSERT INTO public.category_setting (id,created_time,updated_time,created_by,updated_by,"name","code")
VALUES (2,now(),now(),'SYSTEM','SYSTEM','push_notification','push_notification');

INSERT INTO public.category_setting (id,created_time,updated_time,created_by,updated_by,"name","code")
VALUES (4,now(),now(),'SYSTEM','SYSTEM','sms_notification','sms_notification');

INSERT INTO public.category_setting (id,created_time,updated_time,created_by,updated_by,"name","code")
VALUES (5,now(),now(),'SYSTEM','SYSTEM','show_onboarding','how_onboarding');

INSERT INTO public.category_setting (id,created_time,updated_time,created_by,updated_by,"name","code")
VALUES (6,now(),now(),'SYSTEM','SYSTEM','widget_order','widget_order');