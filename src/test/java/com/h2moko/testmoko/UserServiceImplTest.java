package com.h2moko.testmoko;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.h2moko.testmoko.entity.Users;
import com.h2moko.testmoko.users.application.UserServiceImpl;
import com.h2moko.testmoko.users.web.in.UserCreateRequest;
import com.h2moko.testmoko.users.web.in.UserRepository;
import com.h2moko.testmoko.utils.exceptions.AlreadyExistException;
import com.h2moko.testmoko.utils.exceptions.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.assertj.core.api.Assertions.assertThat;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;


    @InjectMocks
    private UserServiceImpl userServiceImpl;

    private Users users;

    private UserCreateRequest userCreateRequest;




    @BeforeEach public void setup(){
        users = Users.builder()
                .ssn("0000000000000123")
                .birthDate(new Date())
                .updatedDate(new Date())
                .createdDate(new Date())
                .createdBy("SYSTEM")
                .updatedBy("SYSTEM")
                .id(1L)
                .isActive(true)
                .firstName("firstname")
                .middleName("middleName")
                .familyName("familyname")
                .deletedDate(null)
                .userSettings(new ArrayList<>())
                .build();

    }

    @Test
    public void get_all_from_repository(){


        Users users2 = Users.builder()
                .ssn("0000000000000123")
                .birthDate(new Date())
                .updatedDate(new Date())
                .createdDate(new Date())
                .createdBy("SYSTEM")
                .updatedBy("SYSTEM")
                .id(2L)
                .isActive(true)
                .firstName("firstname")
                .middleName("middleName")
                .familyName("familyname")
                .deletedDate(null)
                .userSettings(new ArrayList<>())
                .build();

        given(userRepository.getAllByDeletedDateIsNull()).willReturn(List.of(users,users2));
        List<Users> usersList = userServiceImpl.getAll();

        // then - verify the output
        assertThat(usersList).isNotNull();
        assertThat(usersList.size()).isEqualTo(2);
    }

    @Test
    public void get_user_by_id(){
        // given
        given(userRepository.getByIdAndDeletedDateIsNull(1L)).willReturn(Optional.of(users));
        org.junit.jupiter.api.Assertions.assertThrows(NotFoundException.class, () -> {
            throw new NotFoundException("user id not found");
        });

        // when
        Users savedEmployee = userServiceImpl.getDetail(users.getId());

        // then
        assertThat(savedEmployee).isNotNull();
    }

    @Test
    public void get_user_update_by_id(){

        given(userRepository.save(users)).willReturn(users);
        users.setFirstName("john");
        users.setMiddleName("key");
        given(userRepository.getByIdAndDeletedDateIsNull(1L)).willReturn(Optional.of(users));
        org.junit.jupiter.api.Assertions.assertThrows(NotFoundException.class, () -> {
            throw new NotFoundException("user id not found");
        });
        userCreateRequest = UserCreateRequest.builder()
                .first_name("john")
                .last_name("key")
                .build();
        Users userUpdate = userServiceImpl.update(1L,userCreateRequest);
        users.setFirstName(userUpdate.getFirstName());
        users.setMiddleName(userUpdate.getMiddleName());

        assertThat(users.getFirstName()).isEqualTo("john");
        assertThat(users.getMiddleName()).isEqualTo("key");

    }

    @Test
    public void delete_user_by_id(){
        long userId = 1L;
        given(userRepository.findById(1L)).willReturn(Optional.of(users));
        org.junit.jupiter.api.Assertions.assertThrows(NotFoundException.class, () -> {
           throw new NotFoundException("user id not found");
        });
        userServiceImpl.delete(userId);
        assertThat(users.isActive()).isEqualTo(false);
        assertThat(users.getDeletedDate()).isNotNull();
    }


    @Test
    public void save_users()  {
        String ssn = "0000000000002123";
        given(userRepository.getBySsnAndDeletedDateIsNull(ssn)).willReturn(Optional.of(users));
        org.junit.jupiter.api.Assertions.assertThrows(AlreadyExistException.class, () -> {
            UserCreateRequest request = UserCreateRequest.builder()
                    .first_name("firstname")
                    .last_name("middleName")
                    .ssn(ssn)
                    .birth_date(new Date())
                    .build();
            userServiceImpl.create(request);
        });

        // then
        verify(userRepository, never()).save(any(Users.class));

    }

    @Test
    public void whenUsingCustomDateSerializer_thenCorrect()
            throws JsonProcessingException, ParseException {

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

        String toParse = "20-12-2014 02:30:00";
        Date date = df.parse(toParse);
        Users users2 = Users.builder()
                .ssn("0000000000000123")
                .birthDate(date)
                .updatedDate(date)
                .createdDate(date)
                .createdBy("SYSTEM")
                .updatedBy("SYSTEM")
                .id(2L)
                .isActive(true)
                .firstName("firstname")
                .middleName("middleName")
                .familyName("familyname")
                .deletedDate(null)
                .userSettings(new ArrayList<>())
                .build();

        ObjectMapper mapper = new ObjectMapper();
        String result = mapper.writeValueAsString(users2);
        assertThat(result);
    }
}
